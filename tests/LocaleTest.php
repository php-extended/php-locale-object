<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-locale-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Locale\Locale;
use PHPUnit\Framework\TestCase;

/**
 * LocaleTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Locale\Locale
 *
 * @internal
 *
 * @small
 */
class LocaleTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Locale
	 */
	protected Locale $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('en_US', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Locale('en_US');
	}
	
}
