<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-locale-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Locale;

use InvalidArgumentException;

/**
 * Locale class file.
 * 
 * This class is a simple implementation of the LocaleInterface.
 * 
 * @author Anastaszor
 */
class Locale implements LocaleInterface
{
	
	/**
	 * Gets the default locale on the system.
	 * 
	 * @return LocaleInterface
	 * @throws InvalidArgumentException 
	 */
	public static function getDefault() : LocaleInterface
	{
		return new self(\Locale::getDefault());
	}
	
	/**
	 * Sets the default locale on the system with the given locale. Returns true
	 * if this is a success, false else.
	 * 
	 * @param LocaleInterface $locale
	 * @return boolean
	 */
	public static function setDefault(LocaleInterface $locale) : bool
	{
		return \Locale::setDefault($locale->__toString());
	}
	
	/**
	 * Returns a correctly ordered and delimited locale ID.
	 * 
	 * <p>
	 * an array containing a list of key-value pairs, where the keys identify
	 * the particular locale ID subtags, and the values are the associated
	 * subtag values.
	 * </p>
	 * <p>
	 * The 'variant' and 'private' subtags can take maximum 15 values
	 * whereas 'extlang' can take maximum 3 values.e.g. Variants are allowed
	 * with the suffix ranging from 0-14. Hence the keys for the input array
	 * can be variant0, variant1, ...,variant14. In the returned locale id,
	 * the subtag is ordered by suffix resulting in variant0 followed by
	 * variant1 followed by variant2 and so on.
	 * </p>
	 * <p>
	 * The 'variant', 'private' and 'extlang' multiple values can be specified
	 * both as array under specific key (e.g. 'variant') and as multiple
	 * numbered keys (e.g. 'variant0', 'variant1', etc.).
	 * </p>
	 * 
	 * @param array<integer, string> $subtags
	 * @return LocaleInterface
	 * @throws InvalidArgumentException
	 */
	public static function composeLocale(array $subtags) : LocaleInterface
	{
		/** @phpstan-ignore-next-line */
		return new self((string) \Locale::composeLocale($subtags));
	}
	
	/**
	 * The canonicalized locale structure according to ICU standards.
	 * 
	 * @var string
	 */
	protected string $_canonical;
	
	/**
	 * Builds a new Locale according to the given locale string. When the 
	 * parser fails, it falls back on the default locale characteristics.
	 * 
	 * @param string $localeStr
	 * @throws InvalidArgumentException
	 */
	public function __construct(string $localeStr)
	{
		$this->_canonical = (string) \Locale::canonicalize($localeStr);
		if('' === $this->_canonical)
		{
			throw new InvalidArgumentException('The given local string "'.$localeStr.'" is not a valid locale.');
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->_canonical;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getName()
	 */
	public function getName() : string
	{
		return \Locale::getDisplayName($this->_canonical, $this->_canonical);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getPrimaryLanguage()
	 */
	public function getPrimaryLanguage() : string
	{
		return (string) \Locale::getPrimaryLanguage($this->_canonical);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getScript()
	 */
	public function getScript() : ?string
	{
		return \Locale::getScript($this->_canonical);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getRegion()
	 */
	public function getRegion() : ?string
	{
		return \Locale::getRegion($this->_canonical);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getKeywords()
	 */
	public function getKeywords() : array
	{
		$keywords = (array) \Locale::getKeywords($this->_canonical);
		$ret = [];
		
		foreach($keywords as $keyword)
		{
			$ret[] = (string) $keyword;
		}
		
		return $ret;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getDisplayScript()
	 */
	public function getDisplayScript(LocaleInterface $inLocale) : string
	{
		return \Locale::getDisplayScript($this->_canonical, $inLocale->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getDisplayRegion()
	 */
	public function getDisplayRegion(LocaleInterface $inLocale) : ?string
	{
		return \Locale::getDisplayRegion($this->_canonical, $inLocale->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getDisplayName()
	 */
	public function getDisplayName(LocaleInterface $inLocale) : ?string
	{
		return \Locale::getDisplayName($this->_canonical, $inLocale->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getDisplayLanguage()
	 */
	public function getDisplayLanguage(LocaleInterface $inLocale) : ?string
	{
		return \Locale::getDisplayLanguage($this->_canonical, $inLocale->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getDisplayVariant()
	 */
	public function getDisplayVariant(LocaleInterface $inLocale) : ?string
	{
		return \Locale::getDisplayVariant($this->_canonical, $inLocale->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::getAllVariants()
	 */
	public function getAllVariants() : array
	{
		$variants = [];
		
		foreach((array) \Locale::getAllVariants($this->_canonical) as $variant)
		{
			$variants[] = (string) $variant;
		}
		
		return $variants;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof LocaleInterface
			&& $this->__toString() === $other->__toString();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Locale\LocaleInterface::toAcceptHttpHeader()
	 */
	public function toAcceptHttpHeader() : string
	{
		return \str_replace('_', '-', $this->_canonical);
	}
	
}
