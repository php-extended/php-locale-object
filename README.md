# php-extended/php-locale-object
A library that implements the php-extended/php-locale-interface package.

![coverage](https://gitlab.com/php-extended/php-locale-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-locale-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-locale-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\Locale\Locale;

$default = Locale::getDefault();

$fr = new Locale('fr_FR');
$en = new Locale('en_US');
$ch = new Locale('zh-Hant-TW');

$fr->equals($en);	// false
$fr->toAcceptHttpHeader();	// fr-FR

```


## License

MIT (See [license file](LICENSE)).
